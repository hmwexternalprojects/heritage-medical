<?php
/**
 * Fires after the main content, before the footer is output.
 *
 * @since 3.10
 */
?>

<footer id="colophon" class="site-footer">
  <?php 
			global $buildy;
			echo $buildy->renderFrontend('58'); 
		?>
</footer><!-- #colophon -->
</div><!-- #page -->


<?php wp_footer(); ?>

</body>

</html>
