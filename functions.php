<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


// Gallery Sizes
add_image_size('article-grid', 720, 540, true );
add_image_size('slider', 2560, 1200, true );
add_image_size('masonry', 2560, 600, true );
add_image_size('blurb-hero', 2560, 600, true );
load_child_theme_textdomain( 'webpack-starter-child', get_stylesheet_directory() . '/languages' );

// Remove dots from excerpt
add_filter('excerpt_more', function($more) {
  return '';
});

function hmw_remove_parent_stuff() {
    wp_dequeue_style( 'hmw-frontend-styles' );
    wp_deregister_style( 'hmw-frontend-styles' );
    wp_dequeue_script( 'hmw-frontend-scripts' );
    wp_deregister_script( 'hmw-frontend-scripts' );
}

add_action( 'wp_enqueue_scripts', 'hmw_remove_parent_stuff', 20 );

function theme_enqueue_styles() {
  // Enqueue Fonts (if any)
  options_google_font_enqueue();

  // Get the theme data
	// $the_theme = wp_get_theme();
  wp_register_script( 'hmw-child-frontend-scripts', get_stylesheet_directory_uri() . '/public/frontend-bundle.js', array('jquery'), null, true );
	wp_localize_script( 'hmw-child-frontend-scripts', 'global_vars', array(
		'admin_ajax_url' => admin_url( 'admin-ajax.php' ),
		'rest_base' => site_url() . '/wp-json/wp/v2/',
    'images_directory' => get_stylesheet_directory_uri()
  ) );
  
	// Loads bundled frontend CSS.
  wp_enqueue_style( 'hmw-child-frontend-styles', get_stylesheet_directory_uri() . '/public/frontend.css' );
	wp_enqueue_style( 'hmw-theme-options', get_stylesheet_directory_uri() . '/public/hmw-theme-options.css' );	
  	
  wp_enqueue_script('hmw-child-frontend-scripts');

  // Open Layers - Maps
	wp_enqueue_script( 'openlayers', "https://cdn.maptiler.com/ol/v6.0.0/ol.js", '', '', true);
	wp_enqueue_script( 'maptiler', "https://cdn.maptiler.com/ol-mapbox-style/v5.0.2/olms.js", '', '', true);
	wp_enqueue_style( 'maptiler', "https://cdn.maptiler.com/ol/v6.0.0/ol.css" );
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );


add_action('main-header:before-toggle', function() {

  ob_start(); ?>

  <div class="main-header__right flex items-center mr-2">
    <ul class="main-header__contact-numbers text-light p-0">
      <li>P: (02) 4422 1210</li>
      <li>F: (02) 4422 5585</li>
    </ul>  
    <a href="#" class="btn btn--outlined">Book an Appointment</a>
  </div>

  <?php

  echo ob_get_clean();
});

/****************************************************************************************************************
* If any of these files don't exist it will revert to parent themes version
*/
include get_theme_file_path( '/inc/shortcodes/default-shortcodes.php' ); // These are the default shortcodes we bundle 
include get_theme_file_path( '/inc/shortcodes/post-shortcodes.php' ); // Everything related to posts
include get_theme_file_path( '/inc/shortcodes/page-shortcodes.php' ); // Everything related to pages
include get_theme_file_path( '/inc/shortcodes/locations.php' ); // Everything related to pages
include get_theme_file_path( '/inc/shortcodes/team.php' ); // Everything related to pages
include get_theme_file_path( '/inc/shortcodes/services.php' ); // Everything related to pages
include get_theme_file_path( '/inc/shortcodes/posts.php' ); // Everything related to pages


/**
 * Get all the registered image sizes along with their dimensions
 *
 * @global array $_wp_additional_image_sizes
 *
 * @link http://core.trac.wordpress.org/ticket/18947 Reference ticket
 * @return array $image_sizes The image sizes
 */
function _get_all_image_sizes() {
	global $_wp_additional_image_sizes;

	$default_image_sizes = array( 'thumbnail', 'medium', 'large' );
	 
	foreach ( $default_image_sizes as $size ) {
		$image_sizes[$size]['width']	= intval( get_option( "{$size}_size_w") );
		$image_sizes[$size]['height'] = intval( get_option( "{$size}_size_h") );
		$image_sizes[$size]['crop']	= get_option( "{$size}_crop" ) ? get_option( "{$size}_crop" ) : false;
	}
	
	if ( isset( $_wp_additional_image_sizes ) && count( $_wp_additional_image_sizes ) )
		$image_sizes = array_merge( $image_sizes, $_wp_additional_image_sizes );
		
	return $image_sizes;
}

function disable_message_load_field( $field ) {
  $field['readonly'] = 1;
    return $field;
  }
add_filter('acf/load_field/name=sub_field_1', 'disable_message_load_field');
add_filter('acf/load_field/name=sub_field_2', 'disable_message_load_field');


function heritage_query_vars( $qvars ) {
    $qvars[] = 'location';
    return $qvars;
}
add_filter( 'query_vars', 'heritage_query_vars' );


if (!function_exists('print_categories')) : 

  function print_categories() {
    $categories_list = get_the_category_list( esc_html__( ', ', 'hmw' ) );
      // var_dump($categories_list);
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="article-grid__cat-links">' . esc_html__( '%1$s', 'hmw' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}
  }

endif;
