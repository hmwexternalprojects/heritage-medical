<?php
 get_header();

 if (!is_front_page(  )) : 

  get_template_part( "template-parts/header", "page");

 endif;
?>

<div <?php post_class(); ?> id="main-content">

  <div class="bmcb-section container">
    <div class="bmcb-row row">
      <div class="bmcb-column col-12">
        <div class="bmcb-postgrid">
          <?= apply_shortcodes('[list-posts perpage="6" pagination_enabled="true" pagination_type="loadmore" offset="1" cols="3"]'); ?>
        </div>
      </div>
    </div>
  </div>

</div> <!-- #main-content -->

<?php
if (function_exists('render_common_globals')) {
  echo render_common_globals();
}
get_footer();
