<?php


if (function_exists('get_field')) {
  add_shortcode( 'locations-map-module', function($atts) {

    $atts = shortcode_atts( [
      ""
    ], $atts);

    $args = [
      "posts_per_page" => -1,
      "post_type" => 'locations',
      "post_status" => "publish",
    ];

	$query = new WP_Query($args); 

    ob_start(); ?>

  <?php if ($query->have_posts()) : ?>
    <div class="locations-map-module__wrapper">
      <div class="container">
        <div class="locations-map-module">
          <ul class="locations-map-module__list">
            <?php while ($query->have_posts()) : $query->the_post(); ?>
            <?php 
            $address = get_field('address');
            $location_colour = get_field('location_colour') ?? 'inherit';
            $address_string = "$address[street_number] $address[street_name_short] $address[city], $address[state_short], $address[post_code]";
            $ID = get_the_ID();
            // print_r(get_field('address')); 
            ?>
            <li class="locations-map-module__list-item">
              <div style="background-color: <?= $location_colour; ?>" class="locations-map-module__list-logo-wrapper">
                <?= get_the_post_thumbnail( esc_attr($ID), 'full', ["class" => "locations-map-module__list-logo"] ); ?>
              </div>
              <div class="locations-map-module__list-details">
                <h4 style="color: <?= sanitize_hex_color($location_colour); ?>" class="locations-map-module__list-details__title"><?= esc_html__(get_the_title(), 'hmw'); ?></h4>
                <span class="locations-map-module__list-details__address"><?= esc_html($address_string ?? null) ?></span>
                <span class="locations-map-module__list-details__phone">P: <?= esc_html(get_field('location_phone')) ?? null ?></span>
              </div>
            </li>
            <?php endwhile; ?>
          </ul>
          <div class="locations-map-module__map"></div>
        </div>
      </div>
    </div>

  <?php endif; ?>

  <?php 
    wp_reset_query();
    return ob_get_clean();
  });
}

add_shortcode( 'get-locations-list', function($atts) {

  $atts = shortcode_atts( [
    "exclude" => null,
    "echo" => true,
  ], $atts );

  $args = array(
        'post_type' => 'locations',
    );
    
    if (isset($atts['exclude'])) {
      $args['post__not_in'] = array_map('intval', explode(',', $atts['exclude']));
    }

    $posts = get_posts($args);

    if (!$atts['echo']) {
      return $posts;
    }

    global $wp;

    ob_start(); ?>

    <ul class="locations-list">
    <?php foreach($posts as $location) : ?>

      <li class="locations-list__item" data-locationid="<?= $location->ID ?>">
        <a style="background-color: <?= sanitize_hex_color(get_field('location_colour', $location->ID)) ?>" href="<?= esc_url(home_url($wp->request) . "/?location={$location->post_name}"); ?>"><?= esc_html__($location->post_title, 'hmw') ?></a>
      </li>

    <?php endforeach; ?>
    </ul>

    <?= apply_shortcodes( "[list-team post_type='team' default_location='{$posts[0]->post_name}']") ?>
    <?php
    wp_reset_query();
    return ob_get_clean();

} );

if (function_exists('get_field')) {
  add_shortcode( 'list-locations', function($atts) {

    $atts = shortcode_atts( [
      ""
    ], $atts);

    $args = [
      "posts_per_page" => -1,
      "post_type" => 'locations',
      "post_status" => "publish",
    ];

	$query = new WP_Query($args); 

    ob_start(); ?>

  <?php if ($query->have_posts()) : ?>
    <ul class="locations-list-module grid grid-lg-2">
      <?php while ($query->have_posts()) : $query->the_post(); ?>
      <?php 
      $address = get_field('address');
      $location_colour = get_field('location_colour') ?? 'inherit';
      $address_string = "$address[street_number] $address[street_name_short] $address[city], $address[state_short], $address[post_code]";
      $ID = get_the_ID();
      // print_r(get_field('address')); 
      ?>
      <li class="locations-list-module__list-item flex flex-col">
        <div style="background-color: <?= sanitize_hex_color($location_colour); ?>" class="locations-list-module__list-logo-wrapper text-center py-3">
          <?= get_the_post_thumbnail( esc_attr($ID), 'full', ["class" => "locations-list-module__list-logo"] ); ?>
        </div>
        <div class="locations-list-module__list-details flex-grow-1 bg-lightgray p-4 text-center">
          <h4 style="color: <?= sanitize_hex_color($location_colour); ?>" class="locations-list-module__list-details__title"><?= esc_html__(get_the_title(), 'hmw'); ?></h4>
          <div class="locations-list-module__list-details__address flex flex-col mb-2">
            <strong>Address:</strong>
            <span><?= esc_html($address_string ?? null) ?> | <a href="#">View Map</a></span>
          </div>
          <?php 
          // Check rows exists.
          if( have_rows('opening_hours') ): ?>
            <div class="locations-list-module__list-details__hours flex flex-col mb-2">
              <strong>Opening Hours:</strong>
              <div class="inline-flex">
                  <ul class="text-center m-0">
                  <?php while( have_rows('opening_hours') ) : the_row(); ?>
      
                    <li class="flex justify-center leading-loose">
                      <?= get_sub_field('days') ?>
                      <?= get_sub_field('hours') ?>
                    </li>

                  <?php endwhile; ?>
                  </ul>  
                  
                </div>
            </div>
          <?php endif; ?>
          <div class="locations-list-module__list-details__contact flex flex-col">
            <strong>Contact:</strong>
            <div class="inline-flex">
              <span class="locations-list-module__list-details__phone">Ph: <?= esc_html(get_field('location_phone')) ?? null ?></span>
              <i class="las la-circle text-xs"></i>
              <span class="locations-list-module__list-details__fax">Fax: <?= esc_html(get_field('location_fax')) ?? null ?></span>
            </div>
          </div>
        </div>
      </li>
      <?php endwhile; ?>
    </ul>

  <?php endif; ?>

  <?php 
    wp_reset_query();
    return ob_get_clean();
  });
}
