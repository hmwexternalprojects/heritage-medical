<?php

add_shortcode('list-categories', function() {

  $args = array(
        'child_of'            => 0,
        'current_category'    => 0,
        'depth'               => 0,
        'echo'                => 0,
        'exclude'             => '',
        'exclude_tree'        => '',
        'feed'                => '',
        'feed_image'          => '',
        'feed_type'           => '',
        'hide_empty'          => 1,
        'hide_title_if_empty' => true,
        'hierarchical'        => true,
        'order'               => 'ASC',
        'orderby'             => 'menu_order',
        'separator'           => ',',
        'show_count'          => 0,
        'show_option_all'     => '',
        'show_option_none'    => __( 'No categories' ),
        'style'               => 'list',
        'taxonomy'            => 'category',
        'title_li'            => false,
        'use_desc_for_title'  => 1,
    );
    
  $categories_list = get_categories( $args );
  if ( $categories_list ) {
    $cleanedCats = array_map(function($cat) {
      return $cat->slug !== 'uncategorized' ? $cat : null;
    }, $categories_list);

    if (!empty($cleanedCats)) :

      // var_dump($cleanedCats);

    ob_start(); ?>

    <span class="page-header__categories-list page-header__categories-list--all">

      <?php foreach($cleanedCats as $index=>$cat) : 
        $cat_name = $cat->name; ?>
        <?= sprintf( '<a href="%1$s">%2$s</a>', get_category_link( $cat ), esc_html__( $cat_name, 'hmw' )); ?>
      <?php endforeach; ?>

    </span>

    <?php 
      return ob_get_clean();
    else:
      return 'No Categories Found';
    endif;

    /* translators: 1: list of categories. */
    // return printf( '<span class="cat-links">' . esc_html__( '%1$s', 'hmw' ) . '</span>', $cleanedCats); // WPCS: XSS OK.
  }
  return 'Nope';
});
