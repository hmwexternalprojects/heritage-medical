<?php 
if (function_exists('get_field')) :

add_shortcode( 'service-fee-table', function() {

  $ID = get_the_ID();
  ob_start();
  ?>
  
    <?php 
    // Check rows exists.
    if( have_rows('service_fee_table', $ID) ) : 
      // Get the first item in the array to extract table row keys
      $table_row_keys = array_keys(get_field('service_fee_table', $ID)[0]['service_item']); 
    ?>
    <table>
      <?php if (!empty($table_row_keys)) : ?>
        <thead>
          <tr>
            <?php foreach($table_row_keys as $table_header) : 
              $row_name = ucwords(str_replace('_', ' ', str_replace('__', ' / ', $table_header))); ?>
              <th><?= esc_html__($row_name, 'hmw') ?></th>       
            <?php endforeach; ?>
          </tr>
        </thead>
      <?php endif; ?>
    <tbody>
      <?php while( have_rows('service_fee_table', $ID) ) : the_row(); ?>
        <tr>
          <?php foreach($table_row_keys as $table_row) : 
              $service_item = get_sub_field('service_item'); 
              $row_name = ucwords(str_replace('_', ' ', str_replace('__', ' / ', $table_row)));
              ?>
              <td data-column="<?= esc_attr($row_name) ?>">
                <?= esc_html(is_numeric($service_item[$table_row]) ? "\${$service_item[$table_row]}" : $service_item[$table_row]); ?>
            </td>
            <?php endforeach; ?>
        </tr>
      <?php endwhile; ?>
    </tbody>
  </table>
  <?php 
    // No value.
    else :
        echo 'No data to show at the moment';
    endif; ?>

  <?php
  return ob_get_clean();
});

endif; // Get_field exists
