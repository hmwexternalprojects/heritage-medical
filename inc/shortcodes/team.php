<?php
// [list-posts] This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
if (!function_exists('list_team_shortcode')) {
  function list_team_shortcode($atts, $request) {

    $atts = shortcode_atts( [
      "perpage" => 6,
      "cols" => 3,
      "paged" => true,
      "orderby" => "post_title",
      "order" => "ASC",
      "offset" => 0,
      "default_location" => null
    ], $atts );

    $location_slug = $atts['default_location'];
    
    if (get_query_var('location')) {
      $location_slug = get_query_var('location');
    }

	$current_page = get_query_var('paged');
	$current_page = max( 1, $current_page );
  
	$offset_start = $atts['offset'];
	$offset = ( $current_page - 1 ) * $atts['perpage'] + $offset_start;
	
	$args = [
    "posts_per_page" => $atts['perpage'],
		"offset" => $offset,
		"post_type" => 'team',
		"post_status" => "publish",
    "orderby" => $atts['orderby'],
    "order" => $atts['order'],
    "paged" => $current_page
	];
  
  if (isset($location_slug)) :
    $location = get_page_by_path( $location_slug, OBJECT, 'locations' );
    $location_title = $location->post_title;
  endif;
  
  if (isset($location)) :
    $args['meta_query'] = array(
      array(
        'key'     => 'location',
        'value'   => $location->ID,
        'compare' => 'LIKE'
        )
      );
    endif;
    
    
    $query = new WP_Query($args); 

    $pagination = $query->found_posts > 6;	
    
	$total_rows = max( 0, $query->found_posts - $offset_start );
	$total_pages = ceil( $total_rows / $atts['perpage'] );

  $template_name = $category_name ? "{'team'}-{$category_name}" : 'team';
  $template = locate_template( "template-parts/content-{$template_name}.php");
  
  $location_colour = get_field('location_colour', $location->ID) ?? 'black';
  
  
	ob_start();
	if ($query->have_posts()) : ?>

<?php if (isset($location_title)) : ?>
  <h2 class="location-title"><?= $location->post_title; ?></h2>
<?php endif; ?>
<div id="team-modal" class="modal" aria-hidden="true" data-micromodal-close>

  <div class="modal-inner" tabindex="-1">
    <i class="modal-close las la-times" aria-label="Close modal" data-micromodal-close></i>

    <div role="dialog" aria-modal="true" aria-labelledby="team-modal__title" >
      <header style="background: <?= sanitize_hex_color($location_colour) ?>;">
        <h2 class="team-modal__title"></h2>
        <h3 class="team-modal__sub-title"></h3>
      </header>
      
      <svg class="modal-loader spinner" viewBox="0 0 50 50"><circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle></svg>

      <div class="team-modal__content-wrapper flex -mx-2 p-4">
        <img class="team-modal__image px-2" >
        <div class="team-modal__content px-2"></div>
      </div>

    </div>
  </div>
</div>
<div class="article-grid article-grid__<?= esc_attr($template_name) ?> grid grid-lg-<?php echo esc_attr($atts['cols']); ?>">
  <?php while ($query->have_posts()) : 
				$query->the_post(); 
				$ID = get_the_ID();
				$index = $query->current_post; 
				$postURL = get_permalink(); 
        ?>

  <?php 
  if ($template):
    // Include like this to get access to variables in this scope
    include($template); 
  else: 
    get_template_part('template-parts/content', 'post');
  endif;
  ?>

  <?php endwhile; ?>
</div>

<?php if ($pagination) : ?>
<div class="pagination">
  <?php echo paginate_links( array(
					'total'   => $total_pages,
					'current' => $current_page,
					'prev_text' => 'Prev',
					'next_text' => 'Next'
				) ); ?>
</div>
<?php endif; 
		// pagination($total_pages) 
		?>

<?php else: ?>

  <p>Members for <?= esc_html__($location_title, 'hmw') ?> coming soon</p>
  <?php
  endif; 
	wp_reset_postdata();
	return ob_get_clean();
}
  add_shortcode('list-team', 'list_team_shortcode');
}
