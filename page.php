<?php
 get_header();

 if (!is_front_page(  )) : 

  get_template_part( "template-parts/header", "page");

 endif;
?>

<div <?php post_class(); ?> id="main-content">

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-wrap">

			<?php
				the_content();
			?>

		</div>

	<?php endwhile; ?>
</div> <!-- #main-content -->

<?php

get_footer();
