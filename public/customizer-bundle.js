/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/customizer.js":
/*!***************************!*\
  !*** ./src/customizer.js ***!
  \***************************/
/***/ (function() {

eval("/**\r\n * File customizer.js.\r\n *\r\n * Theme Customizer enhancements for a better user experience.\r\n *\r\n * Contains handlers to make Theme Customizer preview reload changes asynchronously.\r\n */\n(function ($) {\n  // Site title and description.\n  wp.customize('blogname', function (value) {\n    value.bind(function (to) {\n      $('.site-title a').text(to);\n    });\n  });\n  wp.customize('blogdescription', function (value) {\n    value.bind(function (to) {\n      $('.site-description').text(to);\n    });\n  }); // Header text color.\n\n  wp.customize('header_textcolor', function (value) {\n    value.bind(function (to) {\n      if ('blank' === to) {\n        $('.site-title, .site-description').css({\n          clip: 'rect(1px, 1px, 1px, 1px)',\n          position: 'absolute'\n        });\n      } else {\n        $('.site-title, .site-description').css({\n          clip: 'auto',\n          position: 'relative'\n        });\n        $('.site-title a, .site-description').css({\n          color: to\n        });\n      }\n    });\n  });\n})(jQuery);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly91bmRlcnNjb3Jlcy8uL3NyYy9jdXN0b21pemVyLmpzPzU0OTQiXSwibmFtZXMiOlsiJCIsIndwIiwiY3VzdG9taXplIiwidmFsdWUiLCJiaW5kIiwidG8iLCJ0ZXh0IiwiY3NzIiwiY2xpcCIsInBvc2l0aW9uIiwiY29sb3IiLCJqUXVlcnkiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsQ0FBQyxVQUFTQSxDQUFULEVBQVk7QUFDWDtBQUNBQyxFQUFBQSxFQUFFLENBQUNDLFNBQUgsQ0FBYSxVQUFiLEVBQXlCLFVBQVNDLEtBQVQsRUFBZ0I7QUFDdkNBLElBQUFBLEtBQUssQ0FBQ0MsSUFBTixDQUFXLFVBQVNDLEVBQVQsRUFBYTtBQUN0QkwsTUFBQUEsQ0FBQyxDQUFDLGVBQUQsQ0FBRCxDQUFtQk0sSUFBbkIsQ0FBd0JELEVBQXhCO0FBQ0QsS0FGRDtBQUdELEdBSkQ7QUFLQUosRUFBQUEsRUFBRSxDQUFDQyxTQUFILENBQWEsaUJBQWIsRUFBZ0MsVUFBU0MsS0FBVCxFQUFnQjtBQUM5Q0EsSUFBQUEsS0FBSyxDQUFDQyxJQUFOLENBQVcsVUFBU0MsRUFBVCxFQUFhO0FBQ3RCTCxNQUFBQSxDQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1Qk0sSUFBdkIsQ0FBNEJELEVBQTVCO0FBQ0QsS0FGRDtBQUdELEdBSkQsRUFQVyxDQWFYOztBQUNBSixFQUFBQSxFQUFFLENBQUNDLFNBQUgsQ0FBYSxrQkFBYixFQUFpQyxVQUFTQyxLQUFULEVBQWdCO0FBQy9DQSxJQUFBQSxLQUFLLENBQUNDLElBQU4sQ0FBVyxVQUFTQyxFQUFULEVBQWE7QUFDdEIsVUFBSSxZQUFZQSxFQUFoQixFQUFvQjtBQUNsQkwsUUFBQUEsQ0FBQyxDQUFDLGdDQUFELENBQUQsQ0FBb0NPLEdBQXBDLENBQXdDO0FBQ3RDQyxVQUFBQSxJQUFJLEVBQUUsMEJBRGdDO0FBRXRDQyxVQUFBQSxRQUFRLEVBQUU7QUFGNEIsU0FBeEM7QUFJRCxPQUxELE1BS087QUFDTFQsUUFBQUEsQ0FBQyxDQUFDLGdDQUFELENBQUQsQ0FBb0NPLEdBQXBDLENBQXdDO0FBQ3RDQyxVQUFBQSxJQUFJLEVBQUUsTUFEZ0M7QUFFdENDLFVBQUFBLFFBQVEsRUFBRTtBQUY0QixTQUF4QztBQUlBVCxRQUFBQSxDQUFDLENBQUMsa0NBQUQsQ0FBRCxDQUFzQ08sR0FBdEMsQ0FBMEM7QUFDeENHLFVBQUFBLEtBQUssRUFBRUw7QUFEaUMsU0FBMUM7QUFHRDtBQUNGLEtBZkQ7QUFnQkQsR0FqQkQ7QUFrQkQsQ0FoQ0QsRUFnQ0dNLE1BaENIIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIEZpbGUgY3VzdG9taXplci5qcy5cclxuICpcclxuICogVGhlbWUgQ3VzdG9taXplciBlbmhhbmNlbWVudHMgZm9yIGEgYmV0dGVyIHVzZXIgZXhwZXJpZW5jZS5cclxuICpcclxuICogQ29udGFpbnMgaGFuZGxlcnMgdG8gbWFrZSBUaGVtZSBDdXN0b21pemVyIHByZXZpZXcgcmVsb2FkIGNoYW5nZXMgYXN5bmNocm9ub3VzbHkuXHJcbiAqL1xyXG5cclxuKGZ1bmN0aW9uKCQpIHtcclxuICAvLyBTaXRlIHRpdGxlIGFuZCBkZXNjcmlwdGlvbi5cclxuICB3cC5jdXN0b21pemUoJ2Jsb2duYW1lJywgZnVuY3Rpb24odmFsdWUpIHtcclxuICAgIHZhbHVlLmJpbmQoZnVuY3Rpb24odG8pIHtcclxuICAgICAgJCgnLnNpdGUtdGl0bGUgYScpLnRleHQodG8pO1xyXG4gICAgfSk7XHJcbiAgfSk7XHJcbiAgd3AuY3VzdG9taXplKCdibG9nZGVzY3JpcHRpb24nLCBmdW5jdGlvbih2YWx1ZSkge1xyXG4gICAgdmFsdWUuYmluZChmdW5jdGlvbih0bykge1xyXG4gICAgICAkKCcuc2l0ZS1kZXNjcmlwdGlvbicpLnRleHQodG8pO1xyXG4gICAgfSk7XHJcbiAgfSk7XHJcblxyXG4gIC8vIEhlYWRlciB0ZXh0IGNvbG9yLlxyXG4gIHdwLmN1c3RvbWl6ZSgnaGVhZGVyX3RleHRjb2xvcicsIGZ1bmN0aW9uKHZhbHVlKSB7XHJcbiAgICB2YWx1ZS5iaW5kKGZ1bmN0aW9uKHRvKSB7XHJcbiAgICAgIGlmICgnYmxhbmsnID09PSB0bykge1xyXG4gICAgICAgICQoJy5zaXRlLXRpdGxlLCAuc2l0ZS1kZXNjcmlwdGlvbicpLmNzcyh7XHJcbiAgICAgICAgICBjbGlwOiAncmVjdCgxcHgsIDFweCwgMXB4LCAxcHgpJyxcclxuICAgICAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgJCgnLnNpdGUtdGl0bGUsIC5zaXRlLWRlc2NyaXB0aW9uJykuY3NzKHtcclxuICAgICAgICAgIGNsaXA6ICdhdXRvJyxcclxuICAgICAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJCgnLnNpdGUtdGl0bGUgYSwgLnNpdGUtZGVzY3JpcHRpb24nKS5jc3Moe1xyXG4gICAgICAgICAgY29sb3I6IHRvXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH0pO1xyXG59KShqUXVlcnkpO1xyXG4iXSwiZmlsZSI6Ii4vc3JjL2N1c3RvbWl6ZXIuanMuanMiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/customizer.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/customizer.js"]();
/******/ 	
/******/ })()
;