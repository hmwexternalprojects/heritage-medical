<?php
 get_header();
 get_template_part( "template-parts/header", "page");
?>

<div <?php post_class(); ?> id="main-content">

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-wrap">

			<div class="bmcb-section container-fluid">
        <div class="container">
          <div class="bmcb-row row ">
            <div class="bmcb-column col-12">
              <div class="bmcb-text bmcb-module entry-content">
                <?php
                  $ID = get_the_ID();
                  $intro = get_field('introduction', $ID) ? get_field('introduction', $ID) : false;

                if ($intro) {
                  echo "<p id='post-intro' class='font-large'>$intro</p>";
                } ?>
                <?php the_content(); ?>   
              </div>
            </div>
          </div>
        </div>
			</div>

		</div>

	<?php endwhile; ?>

	<div class="bmcb-section container ">
		<hr style="height: 4px;" class="bg-lightgray xs:my-1" />
		<div class="bmcb-row row ">
			<div class="bmcb-column col-xs-12">
				<div class="bmcb-code-module bmcb-module">
					<h2>Related stories</h2>
					<?php
						$categories = get_the_category( get_the_ID() );
						echo do_shortcode('[list-posts exclude_cats="'. get_the_ID() .'" perpage="3"]');
					 ?>
				</div>
			</div>
		</div>
	</div>

</div> <!-- #main-content -->

<?php
if (function_exists('render_common_globals')) {
  echo render_common_globals();
}
get_footer();
