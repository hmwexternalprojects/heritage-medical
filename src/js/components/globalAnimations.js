// Import the media query helper if you need to control what devices animations trigger on
// import { isAbove } from '../util/breakpoints'

const animations = {
  fadeAnimation: (el) => {
    let speed = el.dataset.speed
    el.style.transition = `all ${speed}ms ease-out`;
    el.style.opacity = "1";
    el.style.transform = "none";
    el.removeAttribute('data-fade')
  },
  staggerInAnimation: (el) => {
    let children = [...el.children].filter(child => !child.dataset.animated)

    for (let i = 0; i < children.length; i++) {
      let child = children[i]
      let delay = child.dataset.delay
      let speed = child.dataset.speed
      // console.log('delay', i * delay)
      setTimeout(function () {
        child.style.transition = `all ${speed}ms ease-out`;
        child.style.transform = "none" 
        child.style.opacity = "1"
        child.setAttribute('data-animated', true)
      }, i * delay);
    }
  }
}


// The intersection observer to monitor when any animating object is in view
const intersectionObserver = new IntersectionObserver((entries, observer) => {

  entries.forEach(el => {
    if (el.isIntersecting) {

      const dataAtts = Object.keys(el.target.dataset)

      if (dataAtts.includes('animation')) {
        animations[`${el.target.dataset.animation}Animation`](el.target)
      }

      observer.unobserve(el.target);
    }
  })

}, { threshold: 0 });

// The different animations are split into functions to be able to enable/disable
// These on a per-route basis. To enable across all add them in common.js
const enableFadeIn = (target = '[data-fade]') => {
  const fadeInEls = document.querySelectorAll(target)

  if (fadeInEls.length) {
    // Setup the initial positions
    fadeInEls.forEach(el => {
      let userOptions = (el.dataset && el.dataset.fade) ? strToObj(el.dataset.fade) : {},
        options = { x: 0, y: 5, opacity: 0, speed: 1000, ...userOptions };

      el.style.transform = `translate(${options.x}px, ${options.y}px)`;
      el.style.opacity = options.opacity;
      el.setAttribute('data-speed', options.speed)
      el.setAttribute('data-animation', 'fade')
      intersectionObserver.observe(el)
    })
  }
}

// As above
const enableStaggerIn = (target = '[data-staggerin') => {
  const staggerEls = document.querySelectorAll(target)

  if (staggerEls.length) {
    staggerEls.forEach(el => {
      let userOptions = (el.dataset && el.dataset.staggerin) ? strToObj(el.dataset.staggerin) : {},
        options = { x: 0, y: 0, opacity: 0, delay: 400, speed: 1000, ...userOptions };

      [...el.children].forEach(child => {
        if (!child.dataset.animated) {
          child.style.transform = `translate(${options.x}px, ${options.y}px)`;
          child.style.opacity = options.opacity;
          child.setAttribute('data-delay', options.delay);
          child.setAttribute('data-speed', options.speed);
        }
      })
      el.setAttribute('data-animation', 'staggerIn')
      intersectionObserver.observe(el)

    })
  }
}

function strToObj(str) {
  if (!str.length) { return false }
  let objStr = str.match(/{[^}]+}/);

  if (!objStr) { return str; }

  objStr = objStr.toString();
  objStr = objStr.replace(/([a-zA-Z]+):/g, '"$1":');
  return JSON.parse(objStr);
}

export { enableFadeIn, enableStaggerIn }