export const OSM_Map = class {
  constructor(element, source) {
    let styleJson = 'https://api.maptiler.com/maps/6358a61a-5883-4f6d-b087-a3d15ab942d5/style.json?key=yz9m6mJvkL65iq3bxHJp';
    this.osm_map = new ol.Map({
      target: element,
      view: new ol.View({
        center: ol.proj.fromLonLat([133.581035, -25.482951]),
        zoom: 12
      })
    });

    window.olms.apply(this.osm_map, styleJson)

    this.locations = source
    this.markerCoords = []
    this.features = []
    this.marker = null
    this.markerLayers = []

    if (typeof this.locations === 'string') {
      this._fetch_locations(`${global_vars.rest_base}${source}`)
    } else {
      this.add_markers_to_map(this.locations)
    }
    // this.handle_events()
  }

  async _fetch_locations(endpoint) {
    let res = await fetch(endpoint)
    let locations = await res.json();
    if (locations) {
      this.locations = locations
      this.add_markers_to_map(this.locations)
    }
  }

  add_markers_to_map(locations) {
    if (!locations) { return }

    locations.forEach(location => {
      this.add_map_marker(location.acf.address.lng, location.acf.address.lat, location.title.rendered, location.acf.address.address, location.link, location.acf.location_marker)
    })

  }

  add_map_marker(lng, lat, title, address, URL, marker) {

    this.markerCoords.push([parseFloat(lng), parseFloat(lat)])

    if (!marker) {
      // Default marker
      marker = { url: `${global_vars.images_directory}/images/map-marker.png`, ID: 'default', width: 121, height: 161 }
    }

    if (!this.markerLayers[marker.ID]) {
      this.markerLayers[marker.ID] = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: []
        }),
        style: new ol.style.Style({
          image: new ol.style.Icon({
            anchor: [0.5, 0.5],
            anchorXUnits: "fraction",
            anchorYUnits: "fraction",
            src: marker.url,
            size: [marker.width, marker.height], // Set the actual size of the icon (img file dimensions, use an icon double what you need for retina)
            scale: 0.5 // Scale it to 50% (retina)
          })
        })
      });
      this.osm_map.addLayer(this.markerLayers[marker.ID]);
    }

    let feature = new ol.Feature({
      geometry: new ol.geom.Point(ol.proj.fromLonLat([parseFloat(lng), parseFloat(lat)])),
      title,
      address,
      URL
    })

    this.markerLayers[marker.ID].getSource().addFeature(feature)

    this.center_bounds()

  }

  center_bounds() {
    // Fit the map to bounds of all markers
    if (this.markerCoords.length > 1) {
      let boundingExtent = ol.extent.boundingExtent(this.markerCoords);
      boundingExtent = ol.proj.transformExtent(boundingExtent, ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857'));
      this.osm_map.getView().fit(boundingExtent, this.osm_map.getSize());

      // Zoom out 1 level once bounds are set
      let currentZoom = this.osm_map.getView().getZoom();

      // How zoomed in you want it after it fits all the markers on screen
      currentZoom = (Number((currentZoom).toFixed(1)) - 2)

      this.osm_map.getView().setZoom(currentZoom)
    }

    if (this.markerCoords.length === 1) {

      this.osm_map.getView().setCenter(
        ol.proj.transform(
          [
            this.locations[0]['lng'],
            this.locations[0]['lat']
          ],
          'EPSG:4326',
          'EPSG:3857'
        )
      );

      this.osm_map.getView().setZoom(8);

    }

    this.osm_map.updateSize();
  }

  handle_events() {
    this.osm_map.on('singleclick', function (event) {
      let output = ''
      if (this.osm_map.hasFeatureAtPixel(event.pixel) === true) {

        marker = this.osm_map.forEachFeatureAtPixel(event.pixel, function (feature) {
          return feature;
        })

        let title = marker.get('title')
        let address = marker.get('address')
        let url = marker.get('URL')

        let coordinate = event.coordinate;

        output = `
				<h3 class="location-title">${title}</h3>
				<address class="location-address">${address}</address>
				`
        // content.innerHTML = output;
        // overlay.setPosition(coordinate);
      } else {
        // overlay.setPosition(undefined);
        // if (closer) {
        //   closer.blur();
        // }
      }
    });
  }
}
