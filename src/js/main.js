// import Vue from 'vue';

// Auto Find all vue components
// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => {
//   let name = key
//     .split('/')
//     .pop()
//     .split('.')[0];
//   Vue.component(name, files(key).default);
// });


// Control which page JS fires on with this router
import Router from './util/Router';

// Import each page / type here
import home from './routes/home';
import common from './routes/common';
import woocommerce from './routes/woocommerce';
import meetOurTeam from './routes/meetOurTeam';

/** Populate Router instance with DOM routes */
const routes = new Router({
  home,
  common,
  woocommerce,
  pageId394: meetOurTeam
});

// Load Events
window.onload = () => {
  routes.loadEvents()
}

// let app = new Vue({
//   el: '#page'
// });
