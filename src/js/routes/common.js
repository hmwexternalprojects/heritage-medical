import mainMenu from '../components/mainMenu'
import stickyBody from '../components/stickyBody'
import debounce from '../util/debounce';
import { isHome } from '../util/wordpress';
import sitewideMessage from '../components/sitewideMessage'
import loadmore from '../components/ajaxLoadMore'


// import { isAbove } from '../util/breakpoints'

import { OSM_Map } from '../components/osm'

export default {
  init() {
    // Main Menu
    mainMenu({
      move_to_nav: ['.main-header__right'],
      breakpoint: 'lg'
    });

    loadmore();

    // Initialise any map modules
    document.querySelectorAll('.locations-map-module__map').forEach(mapEL => {
      new OSM_Map(mapEL, 'locations') // 'locations is the REST API endpoint for the locations post type'
    })
  },
  finalize() {
    ; (function () {
      let mainHeader = document.getElementById('main-header');
      let headerClone = mainHeader.cloneNode(true);

      // Clone header to measure height while real header is hidden
      copyNodeStyle(mainHeader, headerClone)
      headerClone.style.height = "auto";
      headerClone.style.maxHeight = "unset";

      // Apped clone to body so height is calculated
      document.body.appendChild(headerClone)
      let headerHeight = headerClone.clientHeight;

      // Immediately remove it afterwards
      headerClone.remove();

      // Only set margin-bottom on the homepage as other pages don't want to hide under the header
      if (isHome()) {
        mainHeader.style.marginBottom = `-${headerHeight}px`;
        window.addEventListener('resize', debounce(function () {
          mainHeader.style.marginBottom = `-${headerHeight}px`;
        }, 50))
      }

      let sitewideMessageEL = document.querySelector('.sitewide-message')
      if (sitewideMessageEL) {
        sitewideMessage(sitewideMessageEL)
      } else {
        stickyBody('#main-header')
      }
    })()

    function copyNodeStyle(sourceNode, targetNode) {
      var computedStyle = window.getComputedStyle(sourceNode);
      Array.from(computedStyle).forEach(function (key) {
        return targetNode.style.setProperty(key, computedStyle.getPropertyValue(key), computedStyle.getPropertyPriority(key));
      });
    }
  },
};
