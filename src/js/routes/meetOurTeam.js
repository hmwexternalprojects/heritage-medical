import MicroModal from 'micromodal';

const fetchMember = async (modal, trigger) => {
  console.log(trigger)
  let titleEL = modal.querySelector('.team-modal__title');
  let subTitleEL = modal.querySelector('.team-modal__sub-title');
  let contentEL = modal.querySelector('.team-modal__content');
  let imageEL = modal.querySelector('.team-modal__image');
  let loader = modal.querySelector('.modal-loader');

  loader.style.display = 'block';

  let atts = trigger.dataset;
  let res = await fetch(`${global_vars.rest_base}team/${atts.memberid}/?_embed`);
  let data = await res.json();

  let title = data.title.rendered;
  let content = data.content.rendered;
  let role = data.acf.role || '';
  let titles = data.acf.titles || '';
  let areas_of_interest = data.acf.areas_of_interest;

  let subTitle = role ? role + ' - ' + titles : titles && titles;

  subTitleEL.innerHTML = subTitle;

  loader.style.display = 'none';

  titleEL.innerHTML = title;
  contentEL.innerHTML = content;

  if (areas_of_interest.length) {
    let areas_of_interest_wrapper = document.createElement('div');
    areas_of_interest_wrapper.classList.add('modal-areas-of-interest-list');

    let areas_of_interest_markup = `    
      <h4>Areas of interest</h4>
      <ul>
        ${data.acf.areas_of_interest.split(',').map(aoi => `<li>${aoi}</li>`).join('')}
      </ul>
    `;

    areas_of_interest_wrapper.innerHTML = areas_of_interest_markup;
    contentEL.appendChild(areas_of_interest_wrapper)
  }

  if (data._embedded) {
    imageEL.setAttribute('src', data._embedded["wp:featuredmedia"][0].media_details.sizes["article-grid"].source_url)
    imageEL.style.display = "block"
  }

}

const resetModal = (modal) => {
  modal.querySelector('.team-modal__title').innerHTML = '';
  modal.querySelector('.team-modal__sub-title').innerHTML = '';
  modal.querySelector('.team-modal__content').innerHTML = '';
  modal.querySelector('.team-modal__image').style.display = 'none';
}

export default {
  init() {
    // Prevent Default on anything that is a modal trigger
    document.querySelectorAll('[data-micromodal-trigger]').forEach(el => el.addEventListener('click', e => e.preventDefault()))
  },
  finalize() {
    MicroModal.init({
      onShow: fetchMember,
      onClose: resetModal,
    })
  },
};



