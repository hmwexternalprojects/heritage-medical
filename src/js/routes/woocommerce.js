export default {
  init() {
  },
  finalize() {
    // Set loading spinner on ajax button click
    (function () {
      if (jQuery('.products').length && jQuery('.ajax_add_to_cart').length) {

        jQuery(document).on("sf:ajaxfinish", ".searchandfilter", function () {
          setupCartClicks()
        });

        setupCartClicks()

        function setupCartClicks() {
          jQuery('.ajax_add_to_cart').off('click')
          jQuery('.ajax_add_to_cart').on('click', function (e) {
            e.preventDefault();
            let btnText = jQuery(this).text();
            let spinner = `<svg data-buttontxt="${btnText}" version="1.1" id="loading_spinner" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                <path fill="#fff" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50" transform="rotate(295.42 50 50)">
                <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s" from="0 50 50" to="360 50 50" repeatCount="indefinite"></animateTransform>
                </path>
              </svg>`;
            jQuery(this).html(spinner);
          })
        }
      }
    })();

    // Handle the add to cart alert
    jQuery(document.body).on('added_to_cart', function (e, fragments, cart_hash, this_button) {
      let loadingSpinner = jQuery(this_button).find('#loading_spinner')
      let buttonText = loadingSpinner.data('buttontxt')

      jQuery.ajax({
        type: 'POST',
        url: wc_add_to_cart_params.ajax_url,
        data: {
          'action': 'ajax_added_to_cart_items',
          'added': 'yes',
          'product_id': this_button[0].dataset.product_id
        },
        success: function (response) {
          response = JSON.parse(response)
          let product = response.product;

          Toastify({

            text: `${product.name} was added to your cart`,

            duration: 3000,

            className: 'success',

            destination: response.cart_url

          }).showToast();

          if (loadingSpinner) {
            jQuery(this_button).text(buttonText)
            loadingSpinner.remove();
          }


        },
        error: function (err) {

          Toastify({
            text: `Sorry! Something went wrong when adding that product`,
            duration: 4000,
            className: 'error',
          }).showToast();

          if (loadingSpinner) {
            jQuery(this_button).text(buttonText)
            loadingSpinner.remove();
          }

          throw new Error(`When adding a product to the cart we got this error: `, err)
        }
      });
    });
  },
};
