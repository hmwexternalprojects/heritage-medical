<article  id="post-<?php the_ID(); ?>" <?php post_class("article-grid__article"); ?>>
	<header class="article-grid__header">
    <div class="article-grid__image-wrapper">
      <?php the_post_thumbnail(); ?>
    </div>
	</header>
	<div class="article-grid__body entry-content">
    <?php the_title( '<h2 class="article-grid__title entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );?>
		<?php

      // Most of the time
      echo wp_trim_words(get_the_excerpt(), 20);

		?>
    <div class="article-grid__cta-wrapper">
      <a href="<?php echo $postURL; ?>" class="btn article-grid__cta">
        Read More
      </a>
    </div>
  </div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
