<?php 
  if (function_exists('get_field')) {
    $icon = get_field('service_icon') ?? null;
  }
?>
<div class="article-grid__article">
  <?php if (isset($icon)) : ?>
    <i class="services-grid__icon <?= $icon ?>"></i>
  <?php endif; ?>
  <h3 class="article-grid__title">
    <a href="<?php echo $postURL; ?>">
      <?php the_title(); ?>
    </a>
  </h3>
  <?php if (is_front_page()) : ?>
    <div class="article-grid__body">
      <?= wp_trim_words(get_the_excerpt(), 12); ?>
    </div>
  <?php endif; ?>
  <a href="<?php echo $postURL; ?>" class="article-grid__cta">
    Learn More
  </a>
</div>
