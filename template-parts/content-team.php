<?php 
  if (function_exists('get_field')) {
    $icon = get_field('service_icon') ?? null;
  
    $base_height = 540;
    $variable_height = $base_height + $index;
    // now that we have a random_image_id, lets fetch the image itself.
    $thumbnail = get_the_post_thumbnail( get_the_ID(), 'article-grid', ) ?: "<img src='https://source.unsplash.com/random/720x{$variable_height}'>";
  }
?>
<div class="article-grid__article" style="border-top: 3px solid <?= $location_colour ?>">
  <?php if (isset($thumbnail)) : ?>
    <div class="article-grid__team-image">
      <a href="#" data-micromodal-trigger="team-modal" data-memberid="<?= $ID; ?>"><?= $thumbnail ?></a>
    </div>
  <?php endif; ?>
  <h3 class="article-grid__title">
    <a href="#" data-micromodal-trigger="team-modal" data-memberid="<?= $ID; ?>"><?php the_title(); ?></a>
  </h3>
  <div class="article-grid__body">
    <?= wp_trim_words(get_the_excerpt(), 12); ?>
  </div>
  <button data-micromodal-trigger="team-modal" data-memberid="<?= $ID ?>" class="article-grid__cta">
    Learn More
  </button>
</div>
