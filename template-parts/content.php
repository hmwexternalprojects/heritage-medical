<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

?>

<article  id="post-<?php the_ID(); ?>" <?php post_class("article-grid__article"); ?>>
	<header class="article-grid__header">
    <div class="article-grid__image-wrapper">
      <?php 
      
        $image = get_the_post_thumbnail($ID, 'article-grid');
			  echo sprintf("<a class='article-grid__image-wrapper' href='%s'>%s</a>", get_the_permalink($ID), $image); 
      
      ?>
    </div>
    <?php
    if (function_exists('print_categories')) : 
      print_categories();
    endif;
    ?>
		<?php the_title( '<h2 class="article-grid__title entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
	</header>
    

	<!-- <div class="article-grid__body entry-content">
		<?php
    the_excerpt();
		// the_content( sprintf(
		// 	wp_kses(
		// 		/* translators: %s: Name of current post. Only visible to screen readers */
		// 		__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'hmw' ),
		// 		array(
		// 			'span' => array(
		// 				'class' => array(),
		// 			),
		// 		)
		// 	),
		// 	get_the_title()
		// ) );

		?>
	</div> -->
</article><!-- #post-<?php the_ID(); ?> -->
