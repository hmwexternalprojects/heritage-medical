<?php

$excerpt = get_the_excerpt() ?? null;
$image = get_the_post_thumbnail( get_the_ID(), 'masonry' ) ?? null;

$title = get_the_title();
$intro = $excerpt;

$is_archive = is_archive();
$is_home = is_home();

if ($is_archive && !is_post_type_archive()) : 
  $title = get_the_archive_title();
  $intro = get_the_archive_description();
  // $image_ID = get_field('blog_index_image', 'option');
endif; 

if ($is_home) : 
  $title = single_post_title(false, false);
endif; 

home_t

?>

<div class="page-header">
  <div class="container">
    <?php if(is_single()) : ?>
      <div class="page-header__categories-list">
        <?php print_categories(); ?>
      </div>
    <?php endif; ?>
    <h1 class="page-header__title">
      
      <?= __($title, 'hmw'); ?>
    </h1>
    <div class="page-header__breadcrumbs">
      <?php
        if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
      ?>
    </div>
    <?php if (!$is_archive && !$is_home) : ?>
      <div class="page-header__intro <?= $is_home ? 'page-header__intro--news' : null ?>">
        <?php if (isset($intro)) : ?>
          <?php the_excerpt(); ?>
        <?php endif; ?>
      </div>
    <?php endif; ?>
    <?php if ($is_home) : ?>
      <div class="page-header__intro <?= $is_home ? 'page-header__intro--news' : null ?>">
        <?= apply_shortcodes('[list-categories]') ?>
      </div>
    <?php endif; ?>
  </div>
  <?php if (!$is_archive && !$is_home  && isset($image)) : ?>
    <div class="page-header__image">
      <div class="container">
        <?= $image ?>
      </div>
    </div>
  <?php endif; ?>
  <?php if ($is_home) : ?>
    <div class="bmcb-section py-0 module-style__background-gradient-light">
      <div class="container container--home">
        <div class="bmcb-row row py-0">
          <div class="bmcb-column col-12">
            <div class="bmcb-postgrid">
              <?= apply_shortcodes('[list-posts perpage="1" cols="1" content_template="hero-news"]'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>

  <?php endif; ?>
</div>



